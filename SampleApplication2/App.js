import React, {Component} from 'react';
import {
  TouchableHighlight,
  StyleSheet,
  Text,
  TextInput,
  View,
  Platform,
} from 'react-native';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      isLogined: false,
    };
  }

  inputChangeHandler = (value, name) => {
    this.setState({
      [name]: value,
    });
  };

  login = () => {
    if (this.state.username == 'temp' && this.state.password == '123456') {
      this.setState({isLogined: true});
    } else {
      this.setState({isLogined: false});
    }
  };

  testID = (id) => {
    return Platform.OS === 'android'
      ? {accessible: true, accessibilityLabel: id}
      : {testID: id};
  };

  render() {
    return (
      <View style={LOCAL_STYLES.wrapper} {...this.testID('app-root')}>
        <View style={LOCAL_STYLES.inputContainer}>
          <TextInput
            name="username"
            {...this.testID('username')}
            style={LOCAL_STYLES.input}
            onChangeText={(text) => this.inputChangeHandler(text, 'username')}
          />
        </View>

        <View style={LOCAL_STYLES.inputContainer}>
          <TextInput
            name="password"
            {...this.testID('password')}
            secureTextEntry={true}
            style={LOCAL_STYLES.input}
            onChangeText={(text) => this.inputChangeHandler(text, 'password')}
          />
        </View>

        <Text {...this.testID('loginstatus')}>
          {this.state.isLogined ? 'success' : 'fail'}
        </Text>

        <TouchableHighlight
          style={LOCAL_STYLES.buttonContainer}
          {...this.testID('login')}
          onPress={this.login}>
          <Text style={{color: 'white'}}>Login</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const LOCAL_STYLES = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  inputContainer: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
    width: '80%',
  },
  buttonContainer: {
    height: 45,
    width: 250,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderRadius: 30,
    backgroundColor: '#00b5ec',
  },
});
