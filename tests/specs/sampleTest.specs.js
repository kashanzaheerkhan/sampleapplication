var expect = require("chai").expect;
const {
  addFeature,
  addStory,
  addEnvironment,
  addSeverity,
} = require("@wdio/allure-reporter").default;
var envoirment = require("../common/constant");
describe("Simple App testing", () => {
  // Adding time out to make sure the app is load prior to test is run
  // beforeEach(() => {
  //   $("~app-root").waitForDisplayed(11000, false);
  //   addFeature("Main Root to find");
  //   addStory("search someting in google");
  //   browser.saveScreenshot("./allure-results/app-root.png");
  // });

  it("Valid Login Test", (async) => {
    addEnvironment(envoirment, envoirment);
    addSeverity("critical");
    $("~app-root").waitForDisplayed(11000, false);
    addFeature("Login story");
    addStory("Valid Login");
    $("~username").setValue("temp");
    $("~password").setValue("123456");
    $("~login").click();
    $("~loginstatus").waitForDisplayed(11000);
    const status = $("~loginstatus").getText();
    expect(status).to.equal("success");
    browser.saveScreenshot(
      `./allure-results/success${new Date().getTime()}.png`
    );
  });

  it("Invalid Login Test", (async) => {
    addEnvironment(envoirment, envoirment);
    addSeverity("blocker");
    addFeature("Login story");
    addStory("Invalid Login");
    $("~username").setValue("temp1");
    $("~password").setValue("1234566");
    $("~login").click();
    $("~loginstatus").waitForDisplayed(11000);
    const status = $("~loginstatus").getText();
    expect(status).to.equal("fail");
    browser.saveScreenshot(`./allure-results/fail${new Date().getTime()}.png`);
  });
});
