const { config } = require('../wdio.conf');
const { join } = require('path');
config.capabilities = [
  {
  platformName: 'iOS',
  maxInstances: 1,
  deviceName: 'iPhone 11',
  platformVersion: '13.4',
  orientation: 'PORTRAIT',
  automationName: 'XCUITest',
  // The path to the app
  app: join(process.cwd(), 'SampleApplication.app.zip'),
  'appium:noReset': true,
  'appium:newCommandTimeout': 240,
  }
];

exports.config = config;
