const { config } = require('../wdio.conf');

// ============
// Specs
// ============
config.specs = ['./tests/specs/app*.spec.js'];

config.capabilities = [
  {
    // maxInstances can get overwritten per capability. So if you have an in-house Selenium
    // grid with only 5 firefox instances available you can make sure that not more than
    // 5 instances get started at a time.
    maxInstances: 1,
    appWaitActivity: '*',
    noReset: true
  }
];

exports.config = config;
