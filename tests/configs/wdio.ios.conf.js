

const { config } = require('../wdio.conf');
config.capabilities = [
  {
    'appium:maxInstances': 1,
    'appium:noReset': true,
    'appium:newCommandTimeout': 240,
    'appium:autoAcceptAlerts':true,
    // `automationName` will be mandatory, see
    // https://github.com/appium/appium/releases/tag/v1.13.0
    'appium:automationName': 'XCUITest',
   platformName: 'iOS',
    'appium:idleTimeout':180,
    'appium:noReset': true,
  'appium:newCommandTimeout': 240,
  'appium:autoAcceptAlerts':true,
  "appium:showXcodeLog":false,
  "appium:showIOSLog":false,
  "appium:useNewWDA":false
  }
];
      
exports.config = config;
