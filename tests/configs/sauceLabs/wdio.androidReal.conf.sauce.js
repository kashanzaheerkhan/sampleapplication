const { config } = require("../../wdio.conf");
config.connectionRetryTimeout = 180000;
config.baseUrl = "https://us1.appium.testobject.com/wd/hub";
config.user = "Kashankhan91";
config.testobject_api_key = "DBD317E0B2AA427787B4129648B6230A";
delete config.runner;
delete config.port;
const currentdate = new Date();
const datetime =
  "Test Date: For Android " +
  currentdate.getDate() +
  "/" +
  (currentdate.getMonth() + 1) +
  "/" +
  currentdate.getFullYear() +
  " @ " +
  currentdate.getHours() +
  ":" +
  currentdate.getMinutes() +
  ":" +
  currentdate.getSeconds();
config.capabilities = [
  {
    deviceName: "Samsung Galaxy S9",
    automationName: "UiAutomator2",
    // The api key that has a reference to the app-project in the TO cloud
    testobject_api_key: "DBD317E0B2AA427787B4129648B6230A",
    // The name of the test for in the cloud
    testobject_test_name: "sample-app-mobile",
    browserName: "",
    appiumVersion: "1.17.1",
    deviceOrientation: "portrait",
    platformVersion: "10",
    platformName: "Android",
    app:
      "https://github.com/usmanhayatkhan/appiumapptest/blob/master/app-release.apk?raw=true",
    waitforTimeout: 500,
    commandTimeout: 500,
    name: "Sample application test",
    build: datetime,
    noReset: true,
  },
];

exports.config = config;
