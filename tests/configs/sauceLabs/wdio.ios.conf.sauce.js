const { config } = require("../../wdio.conf");
config.connectionRetryTimeout = 180000;
config.baseUrl =
  "https://Kashankhan91:0cf90f65-806b-451a-97c8-bfc014957e76@ondemand.us-west-1.saucelabs.com:443";
config.user = "Kashankhan91";
config.key = "0cf90f65-806b-451a-97c8-bfc014957e76";
delete config.runner;
delete config.port;
const currentdate = new Date();
const datetime =
  "Test Date: For IOS " +
  currentdate.getDate() +
  "/" +
  (currentdate.getMonth() + 1) +
  "/" +
  currentdate.getFullYear() +
  " @ " +
  currentdate.getHours() +
  ":" +
  currentdate.getMinutes() +
  ":" +
  currentdate.getSeconds();
config.capabilities = [
  {
    browserName: "",
    appiumVersion: "1.17.1",
    deviceName: "iPhone XS Simulator",
    deviceOrientation: "portrait",
    platformVersion: "13.0",
    platformName: "iOS",
    app:
      "https://github.com/kashanzaheerkhan/test/blob/master/SampleApplication2.app.zip?raw=true",
    waitforTimeout: 500,
    commandTimeout: 500,
    name: "app test",
    build: datetime,
  },
];
exports.config = config;
