const { config } = require("../../wdio.conf");
config.connectionRetryTimeout = 180000;
config.baseUrl = "https://us1.appium.testobject.com/wd/hub";
config.user = "Kashankhan91";
config.testobject_api_key = "C1F3115B78134D3DBAD966B3C8A7A68F";
delete config.runner;
delete config.port;
const currentdate = new Date();
const datetime =
  "Test Date: For IOS " +
  currentdate.getDate() +
  "/" +
  (currentdate.getMonth() + 1) +
  "/" +
  currentdate.getFullYear() +
  " @ " +
  currentdate.getHours() +
  ":" +
  currentdate.getMinutes() +
  ":" +
  currentdate.getSeconds();
config.capabilities = [
  {
    testobject_api_key: "C1F3115B78134D3DBAD966B3C8A7A68F",
    // The name of the test for in the cloud
    testobject_test_name: "sample-app-mobile",
    appiumVersion: "1.17.1",
    deviceName: "iPhone XR",
    deviceOrientation: "portrait",
    platformVersion: "13.6",
    platformName: "iOS",
    app:
      "https://github.com/kashanzaheerkhan/test/blob/master/SampleProject.ipa?raw=true",
    waitforTimeout: 500,
    commandTimeout: 500,
    name: "app test",
    build: datetime,
    deviceType: "tablet",
    automationName: "XCUITest",
  },
];
exports.config = config;
