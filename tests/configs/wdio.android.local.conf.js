const { config } = require("../wdio.conf");
const { join } = require("path");
config.connectionRetryTimeout = 180000;

config.capabilities = [
  {
    // maxInstances can get overwritten per capability. So if you have an in-house Selenium
    // grid with only 5 firefox instances available you can make sure that not more than
    // 5 instances get started at a time.
    maxInstances: 1,
    platformName: "Android",
    platformVersion: "10",
    deviceName: "emulator-5554",
    app:
      "D:\\Project\\SampleApplication\\SampleApplication2\\android\\app\\build\\outputs\\apk\\debug\\app-debug.apk",
    appWaitActivity: "*",
    noReset: true,
  },
];

exports.config = config;
